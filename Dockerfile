FROM node:5.4.0
RUN mkdir /code
WORKDIR /code
COPY ./ /code
COPY ./config_prod.json /code/config.json
RUN npm install
CMD npm run-script prod
