# drones-backend

This application is the backend and drone simulator component of the drones application.
The backend is done in Node.js and uses a specific simulator (`drone_simulator`) in conjuction with `socket.io` to create a set of drones which emit a new position every 1500ms.
The Node.js application then uses this new position to populate a MongoDB collection of `drones`, including their historical position. The Node.js application uses `socket.io` to communicate between the drones and the backend and HTTP GET request to provide drone data to the frontend, built with Angular 6.

## Assumptions

- The drones have onboard a version of Android that supports websockets. Websockets are preferred to say, HTTP polling, because of the lower amount of data used than TCP. This is favourable in a cellular connection where using as little data as possible is better. Due to the fact that cellular networks don't provide HTTP proxy redirection, the PORT (`8000`) argument is set on the `sockets.io` url. The frontend API can be accessed at port `3000`. While the Angular application is served on port `80`.

- The drones are initialized when the Node.js application is run, and they do until the application is killed. The number of drones is variable and can be set in `app.js` via the `totalDrones` variable in `config.json`. Currently there are 2 default drones. The same holds true for the `baseSocketUrl` and other values to configure the app.

- The drones have the ability to move between these geo coordinates: `89` < Latitude < `89.000030` and `179` < Longitude < `179.000030`. The GEO position has fixed 5 decimal places. This is so huge variations don't happen which impact speed and movement unrealistically. In future iterations the positioning can be refined to be more sophisticated.

- The drones have unique numeric progressive ids.

- If a drone object has not moved more than one meter in the last ten seconds, then the `has_not_moved` flag gets sets on the object to signal this to the frontend client. This is done in the backend also to reduce the burden on calculating it in the frontend. In the frontend if a drone `has_not_moved` the row containing the drone information is colored `red`.

- To limit the amount of data transferred there are only the last position sent to the frontend in an array. This is assuming a future and further development of the frontend app taking into account a drone's historic data already available in the backend.

## How to run

1. Unzip the repo locally.
2. CD in the `drones-backend` folder.
3. Run `docker build -it drones-backend .` to create the `Docker` image for the backend.
4. Unzip the frontend repo in another folder.
5. CD in the `drones-frontend` folder.
6. Run `docker build -it drones-frontend .` to create the `Docker` image for the frontend.
7. CD back in `drones-backend`.
8. Run `docker-compose up -d` to run the `docker-compose` file, which includes setting up a MongoDB, the frontend and backend images.
9. Visit `http://[MACHINE_IP_ADDRESS]` to access the built `Angular` application.
