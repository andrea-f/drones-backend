var express = require("express");

var app = express();

var logger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var path = require("path");
var fs = require("fs");
// Read in config.
var config = JSON.parse(fs.readFileSync("./config.json", "utf8"));
/*
 * Set up DB connection to Mongo
 */
require("./js/data/database.js");
var mongoose = require("mongoose");
var Drone = mongoose.model("drone");

mongoose.connect(config.mongoUrl);

app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(function (req, res, next) {
    "use strict";
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS, PATCH");
    next();
});

var api_spec = require("./js/routes/api");
app.use("/api", api_spec);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    "use strict";
    var err = new Error("Not Found");
    err.status = 404;
    next(err);
});
app.use(function (err, req, res, next) {
    "use strict";
    res.status(err.status || 500);
    res.json({ error: err.message });
});

app.listen(3000, function () {
  "use strict";
  console.log("Drones service is listening on port 3000!");
});
// Set up socket service
var server = require("http").Server(app);
var io = require("socket.io")(server);
// Imports the socket.io module
require("./js/routes/drone_reporter")(io, Drone);
// Require drone simulator to mimic drones
var drone_simulator = require("./drone_simulator");
// Initialize drones
drone_simulator.run(config.totalDrones, config.baseSocketUrl);
// Listen on socket server
server.listen(config.socketPort);
module.exports = app;