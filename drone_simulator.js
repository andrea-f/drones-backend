/*
 * Drone simulator model
 */
"use strict";

let moveEvery = 1500, // when to emit move in ms
    ioClient = require("socket.io-client"),
    options = {
        transports: ["websocket"],
        "force new connection": true
    };

function move() {
    /*
     * simply return a random number between 88 and 90 for latitude
     * return a random number between 178 and 180 for longituted
     */
    return {
        "lat": (Math.random() * (89.000030 - 89) + 89).toFixed(5),
        "long": (Math.random() * (179.000030 - 179) + 179).toFixed(5)
    };
}

function newPosition() {
    // Random movement
    return [move().lat, move().long]; // ["lat", "long"]
}

function run(drones, baseSocketUrl) {
    /*
     * Initializes the drones in the white blue sky.
     * Also sets the intervals for each drone to
     * get a new random position.
     */
    if (drones < 1) return;
    let droneId = drones,
        droneUrl = baseSocketUrl + "?id=" + droneId,
        drone = ioClient.connect(droneUrl, options);
    function movement() {
        // Move the drone at set intervals
        // Create a new position
        const position = newPosition().join(",");
        // console.log(position)
        drone.emit("message", position);
    }
    setInterval(movement, moveEvery);
    run((drones-1), baseSocketUrl);
}

module.exports.run = run;
module.exports.newPosition = newPosition;
module.exports.move = move;