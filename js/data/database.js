var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var Drone = new Schema({
    drone_id: {
        type: Number,
        unique: true,
        required: true,
        validate: {
            validator: function (v) {
                "use strict";
                return Boolean(typeof v === "number");
            },
            message: "{VALUE} is not a valid drone_id!"
        }
    },
    // Speed is intended as meters per second
    speed: {
        type: Number,
        required: true,
        default: 0,
        validate: {
            validator: function (v) {
                "use strict";
                return Boolean(typeof v === "number");
            },
            message: "{VALUE} is not a valid speed!"
        }
    },
    has_not_moved: {
        type: Boolean,
        required: true,
        default: false,
        validate: {
            validator: function (v) {
                "use strict";
                return Boolean(typeof v === "boolean");
            },
            message: "{VALUE} is not a valid move!"
        }
    },
    locations: {
        type: Array,
        required: true,
        default: [
            {
                lat: String,
                long: String,
                timestamp: String
            }
        ],
        validate: {
            validator: function (v) {
                "use strict";
                return Boolean((Object.prototype.toString.call(v) === "[object Array]") && (v.length > 0));
            },
            message: "{VALUE} is not a valid array!"
        }
    }

});

module.exports = {
    "drone": mongoose.model("drone", Drone)
};
