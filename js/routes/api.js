var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Drone = mongoose.model("drone");

/*
 * Get latest and all news
 */
router.get("/drones", function(req, res, next) {
    "use strict";
    var sort_by = "timestamp";
    Drone.find({}, {locations: {$slice: -1}}, {sort: {sort_by:-1}}, function(err, drones) {
        if (err) {return err;}
        return res.json(drones);
    });
});

module.exports = router;
