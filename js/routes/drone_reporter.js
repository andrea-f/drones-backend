var geolib = require("geolib");

module.exports = (io, Drone) => {
    io.sockets.on("connection", (socket) => {
        var drone_id = socket.handshake.query.id;
        // console.log("connected clientId:" + drone_id);
        socket.on("message", (data) => {
            // console.log("message clientId:" + drone_id);
            // Sending a string so that it consumes less bandwidth
            var coordinates = data.split(","),
                lat = coordinates[0],
                long = coordinates[1];
            var data_obj = {
                "drone_id": drone_id,
                "location": {
                    "lat": lat,
                    "long": long,
                    "timestamp": Date.now()
                }
            };
            // calculates current speed
            // updates the position in db
            updatePosition(data_obj);
        });
    });

    function updatePosition(data) {
        // Update the position in the database
        // based on the drone id set by the socket.
        Drone.findOneAndUpdate(
            {drone_id: data.drone_id},
            {$push:{locations:data.location}},
            {upsert: true, setDefaultsOnInsert: true, new: true},
            (err, drone) => {
                if (err) return err;
                if (drone.locations.length > 1) {
                    var old_loc = drone.locations[drone.locations.length - 2],
                        new_loc = drone.locations[drone.locations.length - 1];
                    // Calculate speed
                    drone.speed = ((geolib.getSpeed(
                        {lat: old_loc.lat, lng: old_loc.long, time: old_loc.timestamp},
                        {lat: new_loc.lat, lng: new_loc.long, time: new_loc.timestamp},
                        {unit:"kmh"}
                    ) * 1000 ) / 60 / 60).toFixed(3); // get speed in meters per hour, then meters per minute, then meters per second
                    // console.log("Speed is: " + drone.speed + " meters per second.");

                    // Check if the drone has moved or not
                    drone.has_not_moved = hasNotMoved(drone);

                    // Save object with new speed
                    drone.save();
                }
                io.sockets.emit("message", "OK");
            }
        );
    }

    function hasNotMoved(drone) {
        // calculates if drone has moved or not
        // in the last 10 seconds for more than 1 meter
        var movement_window = 10000, // 10s
            range = 1; // in meters
            latest_loc = drone.locations[drone.locations.length - 1],
            ten_seconds_ago = latest_loc.timestamp - movement_window,
            // Defaults to a drone moving
            drone_has_not_moved = 0,
            has_not_moved = false,
            distances = [];
        // Check if location has changed in the past 10 seconds
        for (var i=0; i < (drone.locations.length - 1); i++) {
            if (drone.locations[i].timestamp > ten_seconds_ago) {
                var old_loc = {
                    "lat": drone.locations[i].lat,
                    "lng": drone.locations[i].long
                },
                new_loc = {
                    "lat": latest_loc.lat,
                    "lng": latest_loc.long
                };
                // 3rd argument is accuracy in meters
                // 4th argument is precision in meters
                var distance = geolib.getDistance(old_loc, new_loc, 1, 1);
                // Case when distance traveled in the first instance
                // ten seconds ago is less than or equal to 1 meter.
                // console.log("Distance: " + distance);
                if (distance <= range) {
                    drone_has_not_moved ++;
                }
                distances.push(drone_has_not_moved);
            }
        }
        // If the distances are smaller than one meter
        // 1 is added, so if the sum of distances
        // matches the number of intervals
        // it means the drone has never moved.
        if (distances.length === distances.reduce((x, y) => x + y, 0)) {
            has_not_moved = true;
        }
        // console.log("Drone has_not_moved: " + has_not_moved);
        return has_not_moved;

    }
};
