/*global describe, beforeEach, afterEach, ObjectID, Drone*/

var chai = require("chai");
var expect = chai.expect;
var should = chai.should();

var sinon = require("sinon");
var chaiHttp = require("chai-http");
var app = require("../app");
chai.use(chaiHttp);
var io = require("socket.io-client");

// Some tests wide config
var baseSocketUrl = "http://localhost:8000/";
var options = {
  transports: ["websocket"],
  "force new connection": true
};

// Initialize mongodb connection with mongoose
var mongoose = require("mongoose");
var ObjectID = require("mongodb").ObjectID;
var Drone = mongoose.model("drone");


/*
 **********************************
 * DRONES SIMULATION SERVER TESTING
 **********************************
 */

describe("Drones server testing", () => {
    "use strict";
    beforeEach(function () {
    });

    afterEach(() => {
        // Remove each time so we don"t pollute the database
        var id = 999;
        Drone.findOneAndRemove({drone_id: id});
    });

    /* TEST DRONE CAN SEND LOCATION IN URL */
    it("Check that drone can send location coordinates via websocket to drone_reporter", (done) => {
        var drone_id = 999,
            location = [11.3, 2.4], // ["lat", "long"]
            position = location.join(","),
            droneUrl = baseSocketUrl + "?id=" + drone_id;// + "&position=" + position;

        var drone = io.connect(droneUrl, options);

        // Connected drone to server
        drone.on("connect", () => {
            // Send an update position
            drone.emit("message", position);
        });

        drone.on("message", function(msg){
            expect(msg).to.equal("OK");
            drone.disconnect();
            done();
        });
    });

    /* TEST FRONTEND CAN REQUEST LIST OF DRONES USING GET */
    it("Check that frontend can request array of drones", (done) => {
        chai.request(app)
            .get("/api/drones")
            .end((err, res) => {
                expect(res.status).to.equal(200);
                expect(typeof res.body).to.equal("object");
                done();
        });
    }).timeout(6000);

    /* TEST RETURNED DRONE OBJECT HAS ALL KEYS USING GET */
    it("Check which object is returned when making the request for the drones.", (done) => {
        // Add a sample drone
        var sample_drone = new Drone({
            "drone_id": 999,
            "locations": [{lat:11.3, long:2.4, timestamp:123}]
        });
        sample_drone.save();

        // Check that the returned drone has expected properties
        // For the front end to work.
        chai.request(app)
            .get("/api/drones")
            .end((err, res) => {
                var drones = res.body;
                expect(res.status).to.equal(200);
                if (drones.length > 0){
                    expect(drones[0]).to.deep.include.keys("drone_id", "locations", "speed");
                }
                done();
        });
    }).timeout(6000);

    /* TEST DRONE POSITION CAN BE UPDATED */
    it("Check that drone position can be updated", (done) => {
        // Add a sample drone
        var position = {
            lat: 11.3,
            long: 2.4,
            timestamp:123
        },
        drone_id = 999,
        droneUrl = baseSocketUrl + "?id=" + drone_id,
        // Save the drone in the database
        sample_drone = new Drone({
            "drone_id": drone_id,
            "locations": [position]
        });
        sample_drone.save((drone) => {});

        // Connect to the socket
        var drone = io.connect(droneUrl, options);

        // Connected drone to server
        drone.on("connect", () => {
            // Send an update position
            var new_pos = "55.1,44.21";
            drone.emit("message", new_pos);
        });

        drone.on("message", function(msg){
            Drone.findOne({drone_id: sample_drone.drone_id}, (err, drone_obj)=> {
                expect(drone_obj.speed).to.be.above(0);
                drone.disconnect();
                done();
            });
        });
    }).timeout(6000);

    /* TEST DRONE HAS NOT MOVED WORKS CORRECTLY */
    it("Check that drone has_not_moved property works correctly", (done) => {
        // Add a sample drone
        var
            position = {
                lat: 11.3,
                long: 2.4,
                timestamp: Date.now()
            },
            drone_id = 999,
            droneUrl = baseSocketUrl + "?id=" + drone_id,
            // Save the drone in the database
            sample_drone = new Drone({
                "drone_id": drone_id,
                "locations": [position]
            }),
            // Connect to the socket
            drone = io.connect(droneUrl, options);
        sample_drone.save();

        // Connected drone to server
        drone.on("connect", () => {
            // Send an update position
            var new_pos = "11.3,2.4";
            drone.emit("message", new_pos);
        });

        drone.on("message", function(msg){
            Drone.findOne({drone_id: sample_drone.drone_id}, (err, drone_obj)=> {
                expect(drone_obj.has_not_moved).to.equal(true);
                drone.disconnect();
                done();
            });
        });
    }).timeout(6000);
});
