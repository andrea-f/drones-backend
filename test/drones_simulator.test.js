/*global describe, beforeEach, afterEach, ObjectID, Drone*/

var chai = require("chai");
var expect = chai.expect;
var should = chai.should();
var app = require("../app");
var sinon = require("sinon");
var chaiHttp = require("chai-http");

chai.use(chaiHttp);

var drone_simulator;

/*
 **********************************
 * DRONES SIMULATION SERVER TESTING
 **********************************
 */

describe("Drones simulator testing", () => {
    "use strict";
    beforeEach(function () {
        drone_simulator = require("../drone_simulator");

    });

    afterEach(() => {
    });

    /* TEST drone can move */
    it("Test move() functionality latitude above 89 and longitude above", () => {
        const result = drone_simulator.move();
        expect(result.lat).to.be.above(88.9);
        expect(result.long).to.be.above(178.9);
    });

    /* TEST drone can get newPosition */
    it("Test newPosition() functionality to return array of lat and long.", () => {
        const result = drone_simulator.newPosition();
        expect(result[0]).to.be.above(88.9);
        expect(result[1]).to.be.above(178.9);
    });

    /* TEST startDrones is recursively called for the number of total drones */
    it("Test run to be called 2 times for 2 drones", (done) => {
        const totalDrones = 2,
            baseSocketUrl = "http://localhost:8001";
        sinon.spy(drone_simulator, "run");
        drone_simulator.run(totalDrones, baseSocketUrl);
        expect(drone_simulator.run.calledOnce).to.be.true;
        drone_simulator.run.restore();
        done();
    });

    /* TEST startDrones emits new coordinates */
    it("Test run emits new coordinates", (done) => {
        const totalDrones = 1,
            baseSocketUrl = "http://localhost:8001";
        var server = require("http").Server(app);
        var io = require("socket.io")(server);
        server.listen(8001);
        io.sockets.on("connection", function(client) {

            // Listen for message events
            client.on("message", onMessage);

            // Handle a disconnection from the client
            function onMessage(data) {
                expect((data.indexOf(",") !== -1)).to.be.true;
            }
            done();
        });
        drone_simulator.baseSocketUrl = "http://localhost:8001";
        drone_simulator.run(totalDrones, baseSocketUrl);

    });
});